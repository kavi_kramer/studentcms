<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => ''], function () {
    Route::resource('groups', 'GroupController')->middleware('auth');
});

Route::group(['prefix' => ''], function () {
    Route::resource('groups.students', 'StudentController');

    Route::get('/students','StudentController@index')->middleware('auth')->name('students');
    Route::get('/asynch','StudentController@asynch')->name('asynch');
    Route::get('{groupId}/students/{studentId}/', 'AdminController@gate')->name('adminGates');
    Route::get('{groupId}/students/{studentId}/anket', 'StudentController@anket')->name('anket');
    Route::post('storeStudent', 'StudentController@storeStudent')->middleware('auth')->name('storeStudent');
    Route::post('storeStudentPopUp', 'StudentController@storeStudentPopUp')->middleware('auth')->name('storeStudentPopUp');
    Route::post('updateGroup', 'StudentController@updateGroup')->middleware('auth')->name('updateGroup');

    Route::get('/', 'StudentController@home')->name('home');

    Route::post('/students/anket/image','ImageController@upload')->name('image.upload');

    Route::get('/home/rules', 'StudentController@rulesPdf')->name('home.rulesPdf');


});


Route::group(['prefix' => ''], function () {
    Route::resource('groups.students.scores', 'ScoreController');
});

Route::get('/form', function () {
 return view('form');
})->name('form');


Auth::routes();

Route::get('/service/gate', 'AdminController@gate');
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


