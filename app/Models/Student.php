<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Student extends Model
{

    protected $fillable = ['name', 'birthday', 'group_id'];
    protected $guarded = [];

    public function date($request)
    {

        return array(
            'name' => $name = $request->input('name'),
            'birthday' => date('Y-m-d', strtotime($request->input('birthday'))),
            'group_id' => $request->input('group_id'),
            'path' => $request->input('path'),
        );
    }
    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
           $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }
    public function scopeFilter($query, $filter)
    {

        if ($studentName = array_get($filter, 'studentName')) {

            $query->where('name', $studentName);
        }

        if ($groupTitle = array_get($filter, 'groupTitle')) {

            $query->whereHas('group', function ($q) use ($groupTitle) {
                $q->where('groups.title', $groupTitle);
            });
        }
        if ($russianScore = array_get($filter, 'russianScore')) {

            $query->whereHas('scores', function ($q) use ($russianScore) {
                $q->where('scores.score', $russianScore)->where('scores.subject_id', '1');
            });
        }
        if ($mathScore = array_get($filter, 'mathScore')) {

            $query->whereHas('scores', function ($q) use ($mathScore) {
                $q->where('scores.score', $mathScore)->where('scores.subject_id', '2');
            });
        }
        if ($historyScore = array_get($filter, 'historyScore')) {

            $query->whereHas('scores', function ($q) use ($historyScore) {
                $q->where('scores.score', $historyScore)->where('scores.subject_id', '3');
            });
        }
            return ($query);

    }

    public function birthdayConvert($birthday)
    {
        return $birthday = date('d-m-Y', strtotime($birthday));
    }

    public function studentMiddleScore($student, $subject)
    {
        $middleScoreRussian = collect($student->scores->where('subject_id', '=', '1')->first())->get('score');
        $middleScoreMath = collect($student->scores->where('subject_id', '=', '2')->first())->get('score');
        $middleScoreHistory = collect($student->scores->where('subject_id', '=', '3')->first())->get('score');


        $middleScore = round(($middleScoreRussian + $middleScoreMath + $middleScoreHistory) / 3, 1);

        if ($subject == '1') {

            return $middleScoreRussian;
        } elseif ($subject == '2') {

            return $middleScoreMath;
        } elseif ($subject == '3') {

            return $middleScoreHistory;
        } elseif ($subject == '4') {

            return $middleScore;
        }
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function scores()
    {
        return $this->hasMany(Score::class);
    }
}
