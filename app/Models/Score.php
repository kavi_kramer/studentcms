<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Score extends Model
{
    protected $fillable = ['score', 'student_id', 'subject_id'];
    protected $guarded = [];

    public function storeScore($request, $score, $subjectId)
    {

        return array(
            'score' => $request->input($score),
            'subject_id' => $request->input($subjectId),
            'student_id' => $request->input('studentId'),
        );
    }

    public function deleteScore($student, $scoreId)
    {
        $studentId = Score::where('student_id', '=', $student)->first();

        if ($studentId != null) {
            Score::where('id', '=', $scoreId)->first()->delete();
            ++$scoreId;
            Score::where('id', '=', $scoreId)->first()->delete();
            ++$scoreId;
            Score::where('id', '=', $scoreId)->first()->delete();
        }

        return;
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
