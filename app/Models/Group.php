<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Group extends Model
{
    protected $fillable = ['title', 'description'];

    public function indexGroup()
    {
        return ['groups' => Group::with('students.scores')->paginate(6)];
    }

    public function groupMiddleScore($group, $subject)
    {
        $i = 0;
        $middleScoreRussian = [];
        $middleScoreMath = [];
        $middleScoreHistory = [];
        foreach ($group->students as $student) {

            $middleScoreRussian[$i] = $student->scores->where('subject_id', '=', '1')->avg('score');
            $middleScoreMath[$i] = $student->scores->where('subject_id', '=', '2')->avg('score');
            $middleScoreHistory[$i] = $student->scores->where('subject_id', '=', '3')->avg('score');
            ++$i;
        }

        $middleScoreRussian = collect($middleScoreRussian)->reject(function ($value) {
            return $value == null;
        });
        $middleScoreMath = collect($middleScoreMath)->reject(function ($value) {
            return $value == null;
        });
        $middleScoreHistory = collect($middleScoreHistory)->reject(function ($value) {
            return $value == null;
        });

        $middleScoreRussian = round($middleScoreRussian->avg(), 1);
        $middleScoreMath = round($middleScoreMath->avg(), 1);
        $middleScoreHistory = round($middleScoreHistory->avg(), 1);
        $middleScore = round(($middleScoreRussian + $middleScoreMath + $middleScoreHistory) / 3, 1);

        if ($subject == '1') {

            return $middleScoreRussian;
        } elseif ($subject == '2') {

            return $middleScoreMath;
        } elseif ($subject == '3') {

            return $middleScoreHistory;
        } elseif ($subject == '4') {

            return $middleScore;
        }
    }

    public function findFiveScores($group)
    {
        $i = '0';
        $studentName = [];
        foreach ($group->students as $student) {

            $scoreRussian = $student->scores->where('subject_id', '=', '1')->avg('score');
            $scoreMath = $student->scores->where('subject_id', '=', '2')->avg('score');
            $scoreHistory = $student->scores->where('subject_id', '=', '3')->avg('score');
            $middleScore = $scoreRussian + $scoreMath + $scoreHistory;

            if ($middleScore == '15') {

                $studentName[$i] = collect($student)->get('name');
                ++$i;
            }
        }

        return ($studentName);
    }

    public function findGoodScores($group)
    {
        $i = '0';
        $studentName1 = [];
        foreach ($group->students as $student) {

            $scoreRussian = $student->scores->where('subject_id', '=', '1')->avg('score');
            $scoreMath = $student->scores->where('subject_id', '=', '2')->avg('score');
            $scoreHistory = $student->scores->where('subject_id', '=', '3')->avg('score');
            $middleScore = $scoreRussian + $scoreMath + $scoreHistory;

            if ($middleScore < '15' and $middleScore >= '13.5') {
                $studentName = collect($student)->get('name');
                $middleScore = round(($scoreRussian + $scoreMath + $scoreHistory) / 3, 1);
                $studentName1[$i] = collect([$studentName, $scoreRussian, $scoreMath, $scoreHistory, $middleScore]);
                ++$i;
            }
        }

        return $studentName1;
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

}
