<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Group;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create groups.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {

        return $user->role == 'admin';//
    }

    public function store(User $user)
    {

        return $user->role == 'admin';
    }

    public function edit(User $user)
    {

        return $user->role == 'admin';
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Group $group
     * @return mixed
     */
    public function update(User $user)
    {

        return $user->role == 'admin';
    }

    /**
     * Determine whether the user can delete the group.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Group $group
     * @return mixed
     */
    public function delete(User $user)
    {

        return $user->role == 'admin';
    }

}
