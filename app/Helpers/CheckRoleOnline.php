<?php

namespace app\Helpers;

use App\Models\User;

class CheckRoleOnline
{
    public static function checkRoleOnline($id)
    {
        $check = User::where('id', $id)->value('role');

        return $check;
    }
}
