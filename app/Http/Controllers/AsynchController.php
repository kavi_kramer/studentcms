<?php

namespace App\Http\Controllers;

use App\Models\Group;

class AsynchController extends Controller
{
    public function uploadGroup()
    {
        $groups = Group::all();
        foreach ($groups as $key => $group) {
            $groupScores[$key] = collect($group)->merge([
                'middleScore' => $group->groupMiddleScore($group, 4),
                'russianMiddleScore' => $group->groupMiddleScore($group, 1),
                'mathMiddleScore' => $group->groupMiddleScore($group, 2),
                'historyMiddleScore' => $group->groupMiddleScore($group, 3),
            ]);
        }

        return $groupScores;
    }
}
