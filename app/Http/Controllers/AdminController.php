<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;


class AdminController extends Controller
{


    public function gate($group,$student)
    {
        if (Gate::allows('watchAnket')) {
            return redirect()->route('anket',[$group, $student]);
        } else {
            return view('/not_allowed');
        }

        exit;
    }
}