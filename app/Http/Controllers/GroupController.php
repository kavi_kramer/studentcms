<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupRequest;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function __construct(Group $group)
    {
        $this->group = $group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('groups', ($this->group->indexGroup()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

        if ($user->can('create', Group::class)) {

            return view('groups.create', [
                'groups' => [],
            ]);
        } else {

            return view('/not_allowed');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {

        $user = Auth::user();

        if ($user->can('create', Group::class)) {
            $group = Group::create($request->all());

            return redirect()->route('groups.index', $group);
        } else {

            return view('/not_allowed');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $group = Group::with('students.scores')->where('id', '=', $group->id)->first();
        $students = $group->students()->with('scores')->paginate(6);

        return view('groups.show', compact('group', 'students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        $user = Auth::user();

        if ($user->can('edit', Group::class)) {

            return view('groups.edit',
                [
                    'group' => $group,
                ]);
        } else {

            return view('/not_allowed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, Group $group)
    {
        $user = Auth::user();

        if ($user->can('update', Group::class)) {

            $group->update($request->all());

            return redirect()->route('groups.show', $group);
        } else {

            return view('/not_allowed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(
        Group $group
    ) {
        $user = Auth::user();

        if ($user->can('delete', Group::class)) {

            $group->delete();

            return redirect()->route('groups.index');
        } else {

            return view('/not_allowed');
        }
    }
}
