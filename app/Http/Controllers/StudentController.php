<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Models\Group;
use App\Models\Score;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class StudentController extends Controller
{
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    public function index()
    {

        return view('students', ['students' => Student::with('group','scores')->paginate(7)]);
    }

    public function asynch()
    {
        return view('asynch');
    }

    public function home(Request $request)
    {

        $students = Student::filter($request)->with('group','scores')->get();

        $perPage=$request->perPage;
        if ($perPage == null)
        {$perPage='10';}

        $groupsFilter = Group::get();
        $studentsFilter = Student::get();

        $s = $this->student->paginateCollection($students, $perPage, 'home');

        return view('home', ['students' => $s, 'filter' => $request,'studentsFilter'=>$studentsFilter,'groupsFilter'=>$groupsFilter]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($groupId)
    {

        return view('groups.students.create')->with('groupId', $groupId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function anket($group, $student)
    {
        return view('students/anket', compact('group', 'student'));
    }

    public function storeStudent(StudentRequest $request)
    {
        $user = Auth::user();

        if ($user->can('create', Group::class)) {

            $studentId = Student::create($this->student->date($request))->id;
            $score = new Score();
            $request->merge(['studentId' => $studentId]);

            Score::create($request->merge($score->storeScore($request, 'score1', 'subjectId1'))->only('score',
                'student_id', 'subject_id'));
            Score::create($request->merge($score->storeScore($request, 'score2', 'subjectId2'))->only('score',
                'student_id', 'subject_id'));
            Score::create($request->merge($score->storeScore($request, 'score3', 'subjectId3'))->only('score',
                'student_id', 'subject_id'));

            return redirect()->route('students');
        } else {

            return view('/not_allowed');
        }
    }
    public function storeStudentPopUp(Request $request)
    {
        $user = Auth::user();

        if ($user->can('create', Group::class)) {

            $studentId = Student::create($this->student->date($request))->id;
            $request->merge(['studentId' => $studentId]);

            return redirect()->route('students');
        } else {

            return view('/not_allowed');
        }
    }
    public function rulesPdf()
    {
        $view = view('home.rules_pdf');
        $html = $view->render();
        PDF::SetFont('dejavusans', '', 14, '', true);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('rules.pdf');
    }
    public function store(StudentRequest $request)
    {
        Student::create($this->student->date($request));

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit($groupId, $studentId)
    {
        $student = Student::find($studentId);
        $score = Student::with('scores')->where('id', '=', $studentId)->first();

        return view('groups.students.edit')->with(compact('student', 'groupId', 'score'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, Group $group, Student $student)
    {

        Student::where('id', $student->id)->update($student->date($request));

        return redirect()->back();
    }

    public function updateGroup(StudentRequest $request, Group $group, Student $student)
    {

        Student::where('id', $request->student_id)->update(['group_id' => $request->group_id]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group, Student $student)
    {
        $user = Auth::user();

        if ($user->can('delete', Group::class)) {

            $student->delete();

            return redirect()->back();
        } else {

            return view('/not_allowed');
        }
    }
}
