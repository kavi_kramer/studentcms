<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Models\Student;

class ImageController extends Controller
{
   public function upload(ImageRequest $request)
   {

        $path = $request->file('image')->store('uploads', 'public');

        Student::where('id', $request->student_id)->update(['path'=>$path]);

        $student=$request->student_id;
        $group=$request->group_id;

        return view('students.anket', compact('group','student','path'));
   }
}
