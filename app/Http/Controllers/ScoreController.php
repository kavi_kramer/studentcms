<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScoreRequest;
use App\Models\Group;
use App\Models\Score;
use App\Models\Student;
use Illuminate\Http\Request;

class ScoreController extends Controller
{
    public function __construct(Score $score)
    {
        $this->score = $score;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group, Student $student)
    {

        return view('groups.students.scores.index', compact('group', 'student'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScoreRequest $request)
    {
        $studentId = Score::where('student_id', '=', $request->input('studentId'))->first();

        if ($studentId == null) {
            Score::create($request->merge($this->score->storeScore($request, 'score1', 'subjectId1'))->only('score',
                'student_id', 'subject_id'));
            Score::create($request->merge($this->score->storeScore($request, 'score2', 'subjectId2'))->only('score',
                'student_id', 'subject_id'));
            Score::create($request->merge($this->score->storeScore($request, 'score3', 'subjectId3'))->only('score',
                'student_id', 'subject_id'));
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Score $score
     * @return \Illuminate\Http\Response
     */
    public function edit($group, $student, $scoreId1)
    {

        return view('groups.students.scores.edit', compact('student', 'group', 'scoreId1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Score $score
     * @return \Illuminate\Http\Response
     */
    public function update(ScoreRequest $request, $group, $student, $score)
    {
        $studentId = Score::where('student_id', '=', $student)->first();

        if ($studentId != null) {
            $scoreId1 = $score;
            $scoreId2 = $score + 1;
            $scoreId3 = $score + 2;

            Score::where('id', $scoreId1)->update($this->score->storeScore($request, 'score1', 'subjectId1'));
            Score::where('id', $scoreId2)->update($this->score->storeScore($request, 'score2', 'subjectId2'));
            Score::where('id', $scoreId3)->update($this->score->storeScore($request, 'score3', 'subjectId3'));
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Score $score
     * @return \Illuminate\Http\Response
     */
    public function destroy($group, $student, $scoreId)
    {
        $this->score->deleteScore($student, $scoreId);

        return redirect()->back();
    }
}
