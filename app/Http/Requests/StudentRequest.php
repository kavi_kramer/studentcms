<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'group_id' => 'required',
            'name' => 'required',
            'birthday' => 'required',
            'score1' => 'required',
            'score2' => 'required',
            'score3' => 'required',
        ];
    }

    public function messages()
    {

        return [
            'name.required' => 'Вы не ввели имя!',
            'birthday.required' => 'Вы не ввели дату рождения!',
        ];
    }
}
