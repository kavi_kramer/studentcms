<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'score1' => 'required',
            'subjectId1' => 'required',
            'score2' => 'required',
            'subjectId2' => 'required',
            'score3' => 'required',
            'subjectId3' => 'required',
        ];

    }
}
