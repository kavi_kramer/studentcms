<script>
    $.ajax({
        type: "POST",
        url: "{{ route('asynchGroup') }}",
        // data: fd,
        processData: false,
        contentType: false,

        success: function (response) {
            $('<table class="table table-striped">').appendTo('#content');
            $(' <thead>').appendTo('.table');
            $(' <th class="text-center">Название группы</th>').appendTo('.table');
            $(' <th class="text-center">Описание</th>').appendTo('.table');
            $(' <th class="text-center">Успеваемость</th>').appendTo('.table');
            $(' <th class="text-center">Ср. балл Русский язык</th>').appendTo('.table');
            $(' <th class="text-center">Ср. балл Математика</th>').appendTo('.table');
            $(' <th class="text-center">Ср. балл История</th>').appendTo('.table');
            $(' <tbody>').appendTo('.table');
            $.each(response, function (index, value) {
                console.log(value);
                $('<tr>', {id: 'id' + index + ''}).appendTo('.table');
                $('<td class="text-center">' + value.title + '</th>').appendTo('#id' + index + '') +
                $('<td class="text-center">' + value.description + '</th>').appendTo('#id' + index + '')
                $('<td class="text-center">' + value.middleScore + '</th>').appendTo('#id' + index + '')
                $('<td class="text-center">' + value.russianMiddleScore + '</th>').appendTo('#id' + index + '')
                $('<td class="text-center">' + value.mathMiddleScore + '</th>').appendTo('#id' + index + '')
                $('<td class="text-center">' + value.historyMiddleScore + '</th>').appendTo('#id' + index + '')
            });

        },
        error: function () {
            alert(2);
        },
    });
</script>
