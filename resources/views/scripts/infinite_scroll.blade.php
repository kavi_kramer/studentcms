<script>

    $('#infinite').infiniteScroll({
// options
        path: '.pagination__next',
        append: '.post',
        history: false,
        status: '.status',
        hideNav: '.pagination',

    }).infiniteScroll('loadNextPage')
</script>
