<?php

$total=$paginate->total();
$perPage = $paginate->perPage();
$lastPage = ceil($total/$perPage);
$curPage = $paginate->currentPage();
?>
<nav>
    <ul class="pagination">
        <?php $url = $paginate->url(1) ?>
        <li @if($curPage ==1 )class="page-item disabled"@endif>
            <a class="page-link" href="{{ $url }}">
                Previous
            </a>
        </li>
        <?php $p=1 ?>
        @while($p<=$lastPage)
            <?php $url = $paginate->url($p) ?>
            <li  @if($curPage == $p) class="page-item active" @endif>
                <a class="page-link" href="{{ $url }}">
                    {{ $p }}
                        <span class="sr-only">(current)</span>
                </a>
            </li>
            <?php $p=$p+1; ?>
        @endwhile
        <?php $url = $paginate->url($lastPage) ?>
        <li @if($curPage == $lastPage )class="page-item disabled"@endif>
            <a  class="page-link" href="{{ $url }}" >Next
            </a>
        </li>
    </ul>
</nav>