<?php
if (isset($student->scores[0]->score)) {
    $placeScore1 = $student->scores[0]->score;
    $placeScore2 = $student->scores[1]->score;
    $placeScore3 = $student->scores[2]->score;
} else {
    $placeScore1 = "Русский язык";
    $placeScore2 = "Математика";
    $placeScore3 = "История";
}
?>
@if (isset($student->scores[0]->score))
    <form class="form-horizontal" action="{{route('groups.students.scores.update', [$student->group->id,$student,$student->scores[0]->id])}}"
          method="post">
        <input type="hidden" name="_method" value="put">
        @else
            <form class="form-horizontal"
                  action="{{ route('groups.students.scores.store', [$student->group->id,$student->id]) }}"
                  method="post">
                @endif
                <input type="hidden" name="studentId" value="{{$student->id}}">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-center">Изменить оценки</h4>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="score1" placeholder="{{$placeScore1}}"
                               value="{{$score[0]}}">
                        <input type="hidden" name="subjectId1" value="{{$subjectId[0]='1'}}">
                        <br>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="score2" placeholder="{{$placeScore2}}"
                               value="{{$score[1]}}">
                        <input type="hidden" name="subjectId2" value="{{$subjectId[1]='2'}}">
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="score3" placeholder="{{$placeScore3}}"
                               value="{{$score[2]}}">
                        <input type="hidden" name="subjectId3" value="{{$subjectId[2]='3'}}">
                        <br>
                    </div>
                    @if (isset($student->scores[0]->score))
                        <div class="col-md-3">
                            <input class="btn btn-primary" type="submit" value="Изменить">
                        </div>
            </form>
            @else
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <input class="btn btn-primary" type="submit" value="Добавить">
                </div>

    </form>
@endif
@if (isset($student->scores[0]->score))
    <div class="col-md-auto">
        <td class="text-right">
            <form onsubmit="if(confirm('Удалить?')){return true}else{return false}"
                  action="{{route('groups.students.scores.destroy',[$student->group->id, $student->id,$student->scores[0]->id])}}"
                  method="post">
                <input type="hidden" name="_method" value="delete">
                {{csrf_field()}}
                <input class="btn btn-danger" type="submit" value="Удалить">
            </form>
        </td>
    </div>
    @endif
    </div>

