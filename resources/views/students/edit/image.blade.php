<div class="container">
    <form action="{{ route('image.upload') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="group_id" value="{{$student->group->id}}">
        <input type="hidden" name="name" value="{{$student->name}}">
        <input type="hidden" name="student_id" value="{{$student->id}}">
        <input type="hidden" name="birthday" value="{{$student->birthday}}">
        {{ csrf_field() }}
        @if (isset($student->path))
        <div class="row">
                <div class="col-md-12">
                    <img class="img-fluid" src="{{asset('/storage/' .$student->path)}}" height="150" width="150" alt="">
                </div>
            </div>
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-primary" type="submit">Изменить</button>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="file" name="image">
                </div>
            </div>
        </div>
@else
            <div class="row">
                <div class="col-md-12">
                    <img class="img-fluid" src="" height="100" width="100" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-primary" type="submit">Загрузка</button>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="file" name="image">
                    </div>
                </div>
            </div>
            @endif
    </form>

</div>