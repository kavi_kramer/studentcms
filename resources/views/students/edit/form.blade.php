<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Добавить студента
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <?php $score = collect(['', '', ''])?>
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <form class="form-horizontal" action="{{route('storeStudentPopUp')}}" method="post">
                {{csrf_field()}}
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Добавление студента</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">
                            <p class="text">Введите имя студента</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-left"><input type="text" class="form-control" name="name"
                                                        placeholder="Полное имя"></p>
                            <input type="hidden" name="description" value="{{'1'}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text">Введите дату рождения</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center"><input type="text" class="form-control date placeholder"
                                                          name="birthday"
                                                          placeholder="Дата рождения"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text">Введите группу</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <select class="form-control" name="group_id">
                                <option value="">Выберите группу</option>
                                @foreach($groups as $group)

                                    <option value="{{$group->id}}">{{ $group->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h6>Ссылки на PDF с правилами</h6>

                    <p><a target="_blank" href="{{ route('home.rulesPdf')}}" class="tooltip-test" title="Tooltip">Правила добавления студента</a>
                        и
                        <a target="_blank" href="{{ route('home.rulesPdf')}}" class="tooltip-test" title="Tooltip">они же</a> только второй ссылкой.</p>
            </div>
            <div class="modal-footer">

                <div class="col-md-6">@include('students.share')</div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <input class="btn btn-primary" type="submit" value="Добавить"></div>

            </div>

        </form>
        </div>
    </div>
</div>
</div>