<form class="form-horizontal" action="{{route('groups.students.update',[$student->group->id, $student])}}" method="post">
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="group_id" value="{{$student->group->id}}">
    <input type="hidden" name="score1" value="{{$student->group->id}}">
    <input type="hidden" name="score2" value="{{$student->group->id}}">
    <input type="hidden" name="score3" value="{{$student->group->id}}">
    {{csrf_field()}}
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">Изменить имя и дату рождения</h4>
            <div class="text-left"><input type="text" class="form-control" name="name"
                                          placeholder="{{$student->name}}">
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <input type="text" class="form-control date"
                   name="birthday"
                   placeholder={{date('d.m.Y', strtotime($student->birthday))}}>
        </div>
        <div class="col-md-4">
            <input class="btn btn-primary" type="submit" value="Изменить">
        </div>
    </div>
</form>