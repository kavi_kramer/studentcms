<form class="form-horizontal" action="{{route('updateGroup',$student)}}" method="post">
    <input type="hidden" name="score1" value="{{$student->group->id}}">
    <input type="hidden" name="score2" value="{{$student->group->id}}">
    <input type="hidden" name="score3" value="{{$student->group->id}}">
    <input type="hidden" name="name" value="{{$student->name}}">
    <input type="hidden" name="birthday" value="{{$student->birthday}}">
    <input type="hidden" name="student_id" value="{{$student->id}}">
    {{csrf_field()}}
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">Изменить группу</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <select class="form-control" name="group_id">
                <option value="">Выберите группу</option>
                @foreach($groups as $group)

                    <option value="{{$group->id}}"{{ $groups }}>{{ $group->title }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-7">
        </div>
        <div class="col-md-4">
            <input class="btn btn-primary" type="submit" value="Изменить">
        </div>
    </div>
</form>