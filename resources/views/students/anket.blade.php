@extends('layouts.app')


@section('content')
    <?php $groups = \App\Models\Group::all();
     $student = \App\Models\Student::where('id', $student)->with('scores','group')->first();
     $score = collect(['', '', '']); ?>
    <br>
    <div class="container">
        <h2>Анкета студента</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Анкета</li>
        </ol>
        <div class="row">
            <div class="col-md-8">
                <div>
                    <h2>{{$student->name}}</h2>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <h3>Дата рождения:</h3>
                <div>
                    {{ $student->birthdayConvert($student->birthday) }}
                </div>
                <br>
                <h3>Учебная группа:</h3>
                <div>
                    {{$student->group->title}}
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-11">
                        <h3 class="text-center">Оценки:</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h5 class="text-center">Русский язык:</h5>
                        <div class="text-center">
                            @if (isset($student->scores[0]->score))
                            {{$student->scores[0]->score}}
                                @else
                                {{"-"}}
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5 class="text-center">Математика:</h5>
                        <div class="text-center">
                            @if (isset($student->scores[1]->score))
                         {{$student->scores[1]->score}}
                            @else
                                {{"-"}}
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h5 class="text-center">История:</h5>
                        <div class="text-center">
                            @if (isset($student->scores[0]->score))
                            {{$student->scores[2]->score}}
                            @else
                                {{"-"}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h3 class="text-center">Фото:</h3>
                @include ('students.edit.image')
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                @include ('students.edit.student')
            </div>
            <div class="col-md-3">
                @include ('students.edit.group')
            </div>
            <div class="col-md-5">
                @include ('students.edit.score')
            </div>
        </div>
    </div>
@endsection