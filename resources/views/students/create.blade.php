<div class="container">
    <h2>Добавление студента</h2>
    <?php $score = collect(['', '', ''])?>
    <form class="form-horizontal" action="{{route('storeStudent')}}" method="post">
        {{csrf_field()}}
        <table class="table table-striped">
            <thead>
            <th class="text-center">Полное имя</th>
            <th class="text-center">Дата рождения</th>
            <th class="text-center">Группа</th>
            <th class="text-center"></th>
            </thead>
            <tbody>
            <tr>
                <td class="text-left"><input type="text" class="form-control" name="name"
                                             placeholder="Полное имя"></td>
                <input type="hidden" name="description" value="{{'1'}}">
                <td class="text-center"><input type="text" class="form-control date placeholder" name="birthday"
                                               placeholder="Дата рождения"></td>

                <td class="text-center">

                        <select class="form-control" name="group_id">
                            <option value="">Выберите группу</option>
                            @foreach($groups as $group)

                                <option value="{{$group->id}}">{{ $group->title }}</option>
                            @endforeach
                        </select>

                </td>
                <td class="text-right"></td>
            </tr>
            </tbody>
            <thead>
            <th class="text-center">Русский язык</th>
            <th class="text-center">Математика</th>
            <th class="text-center">История</th>
            <th class="text-center"></th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <input type="text" class="form-control" name="score1" placeholder="Русский язык"
                           value="{{$score[0]}}">
                    <input type="hidden" name="subjectId1" value="{{$subjectId[0]='1'}}">
                </td>
                <td>
                    <input type="text" class="form-control" name="score2" placeholder="Математика"
                           value="{{$score[1]}}">
                    <input type="hidden" name="subjectId2" value="{{$subjectId[1]='2'}}">
                </td>
                <td class="text-center">
                    <input type="text" class="form-control" name="score3" placeholder="История"
                           value="{{$score[2]}}">
                    <input type="hidden" name="subjectId3" value="{{$subjectId[2]='3'}}">
                </td>
                <td class="text-right"><input class="btn btn-primary" type="submit" value="Добавить"></td>
            </tr>
            </tbody>
        </table>
    </form>
</div>