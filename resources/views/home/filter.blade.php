<br>
<div class="container">
    <form class="form-horizontal" action="{{route('home')}}" method="get">
        {{csrf_field()}}
        <div class="row">

            <div class="col-md-2">
                <label for="groups">Выберите группу</label>
                <select id="groups" class="form-control" name="groupTitle">
                    <option value="">Все группы</option>

                    @foreach($groupsFilter as $group)
                        <option value="{{ $group->title }}">{{ $group->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-2">
                <label for="students">Введите имя студента</label>
                <select id="students" class="form-control" name="studentName">
                    <option value="">Все студенты</option>
                    @if($studentName = array_get($filter, 'studentName')) {
                    <option selected value="{{$filter->studentName}}">{{$filter->studentName}}</option>
                    }
                    @endif
                    @foreach($studentsFilter as $student)
                        <option value="{{ $student->name }}">{{ $student->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-2">
                <label for="scores">Русский язык</label>
                <select id="scores" class="form-control" name="russianScore">
                    <option value="">Любая оценка</option>
                    @if($russianScore = array_get($filter, 'russianScore')) {
                    <option selected value="{{$filter->russianScore}}">{{$filter->russianScore}}</option>
                    }
                    @endif
                    @foreach($scores = [1,2,3,4,5] as $score)
                        <option value="{{$score}}">{{ $score }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="scores1">Математика</label>
                <select id="scores1" class="form-control" name="mathScore">
                    <option value="">Любая оценка</option>
                    @if($mathScore = array_get($filter, 'mathScore')) {
                    <option selected value="{{$filter->mathScore}}">{{$filter->mathScore}}</option>
                    }
                    @endif
                    @foreach($scores = [1,2,3,4,5] as $score)
                        <option value="{{$score}}">{{ $score }}</option>
                    @endforeach

                </select>
            </div>
            <div class="col-md-2">
                <label for="scores2">История</label>
                <select id="scores2" class="form-control" name="historyScore">
                    <option value="">Любая оценка</option>
                    @if($historyScore = array_get($filter, 'historyScore')) {
                    <option selected value="{{$filter->historyScore}}">{{$filter->historyScore}}</option>
                    }
                    @endif
                    @foreach($scores = [1,2,3,4,5] as $score)
                        <option value="{{$score}}">{{ $score }}</option>
                    @endforeach

                </select>
            </div>
            <div class="col-md-1">
                <label for="pagination">Записей</label>
                <select id="pagination" class="form-control" name="perPage">
                    <option value="">По умолчанию</option>
                    @if($perPage = array_get($filter, 'perPage')) {
                    <option selected value="{{$filter->perPage}}">{{$filter->perPage}}</option>
                    }
                    @endif
                    @foreach($onPage = [3,5,7,10,15,20] as $onPage)
                        <option value="{{$onPage}}">{{ $onPage }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <br>
                <span class="pull-right">
                    <td class="text-right"><input class="btn btn-dark" type="submit" value="Фильтр"></td>
                </span>
            </div>
            @include('home.scripts')

        </div>
    </form>
</div>
