@extends('layouts.app')


@section('content')
    <?php $groups = \App\Models\Group::get();?>
    <br>
    <div class="container">
        <h2>Список студентов</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Главная</a></li>

            <li class="breadcrumb-item active" aria-current="page">Студенты</li>
        </ol>

        <table class="table table-striped">
            <thead>
            <th>Полное имя</th>
            <th>Дата рождения</th>
            <th>Группа</th>
            <th class="text-center">Успеваемость</th>
            <th class="text-center">Ср. балл <br>Русский язык</th>
            <th class="text-center">Ср. балл <br>Математика</th>
            <th class="text-center">Ср. балл <br>История</th>
            @if (auth()->user() && (auth()->user()->role == 'admin'))
                <th class="text-right">Анкета</th>
                <th class="text-right">Удалить</th>
            @endif
            </thead>
        </table>
    </div>
    <div class="container" id="infinite">
        <article class="post">
            <table class="table table-striped">
                <tbody>
                @forelse ($students as $student)
                    <tr>
                        <td class="text-left"> {{$student->name}}</td>
                        <td class="text-left"> {{$student->birthdayConvert($student->birthday)}}</td>
                        <td class="text-left"> {{$student->group->title}}</td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 4) == '0') {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 4));
                            }?></td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 1) == null) {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 1));
                            }?></td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 2) == null) {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 2));
                            }?></td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 3) == null) {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 3));
                            }?></td>
                        @if (auth()->user() && (auth()->user()->role == 'admin'))
                            <td class="text-right">
                                <a class="btn btn-default"
                                   href="{{route('adminGates', [$student->group->id, $student->id])}}"><i class="fa
                                     fa-edit"></i></a>
                            </td>
                            <td class="text-right">
                                <form onsubmit="if(confirm('Удалить?')){return true}else{return false}"
                                      action="{{route('groups.students.destroy',[$student->group->id,$student->id])}}"
                                      method="post">
                                    <input type="hidden" name="_method" value="delete">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </td>
                        @endif
                    </tr>
                @empty
                @endforelse
                </tbody>
            </table>
        </article>
            <div class="row">
                <div class="col-md-4">
                    <!--                    --><?php // echo $students->render(); ?>
                    <a hidden class="pagination__next" href="students?page=2">Next</a>
                </div>
                {{--@if (auth()->user() && (auth()->user()->role == 'admin'))--}}
                {{--<div class="col-md-8 col-md-offset-4">--}}
                {{--<span class="pull-right">@include('students.edit.form')</span>--}}
                {{--</div>--}}
            </div>
    {{--@include('students.create')--}}

    {{--@endif--}}
    </div>
@endsection