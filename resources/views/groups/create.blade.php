@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Создание группы</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('groups.index')}}">Группы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Создать</li>
        </ol>

        <hr/>
        <form class="form-horizontal" action="{{route('groups.store')}}" method="post">
            {{csrf_field()}}
            <label for="">Название группы</label>
            <input type="text" class="form-control" name="title" placeholder="Название группы">
            <label for="">Описание</label>
            <input type="text" class="form-control" name="description" placeholder="Описание">
            <hr/>
            <input class="btn btn-primary" type="submit" value="Добавить">
        </form>

    </div>
@endsection