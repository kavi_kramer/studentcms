@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Редактирование группы</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('groups.index')}}">Группы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>


        </ol>

        <hr/>

        <form class="form-horizontal" action="{{route('groups.update',$group)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{csrf_field()}}


            <label for="">Новое название группы</label>
            <input type="text" class="form-control" name="title" placeholder="{{$group->title}}">
            <label for="">Новое описание</label>
            <input type="text" class="form-control" name="description" placeholder={{$group->description}}>
            <hr/>
            <input class="btn btn-primary" type="submit" value="Изменить">
        </form>

    </div>
@endsection