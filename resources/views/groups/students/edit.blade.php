@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Редактирование студента</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('groups.index')}}">Студенты</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактирование</li>

        </ol>

        <hr/>
        <form class="form-horizontal" action="{{route('groups.students.update',[$groupId, $student])}}" method="post">
            <input type="hidden" name="_method" value="put">
            <input type="hidden" name="group_id" value="{{$groupId}}">
            {{csrf_field()}}
            <label for="">ФИО студента</label>
            <input type="text" class="form-control" name="name" placeholder={{$student->name}}>
            <label for="">Дата рождения</label>
            <?php $birthday = date('d.m.Y', strtotime($student->birthday));?>
            <input type="text" class="form-control date" name="birthday" placeholder={{$birthday}} >
            <hr/>
            <input class="btn btn-primary" type="submit" value="Изменить">
        </form>
        <hr/>
        </form>
    </div>
@endsection