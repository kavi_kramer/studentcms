@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Добавление студента</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Добавить</li>
        </ol>

        <hr/>
        <form class="form-horizontal" action="{{route('groups.students.store',[$groupId])}}" method="post">
            <input type="hidden" name="group_id" value="{{$groupId}}">
            {{csrf_field()}}
            <label for="">ФИО студента</label>
            <input type="text" class="form-control" name="name" placeholder="Фамилия Имя Отчество">
            <label for="">Дата рождения</label>
            <input type="text" class="form-control date placeholder" name="birthday"/>
            <hr/>
            <input class="btn btn-primary" type="submit" value="Добавить">
        </form>


    </div>
@endsection