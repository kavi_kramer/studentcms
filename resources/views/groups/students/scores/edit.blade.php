@extends('layouts.app')


@section('content')

    <div class="container">

        <h2>Оценки студента</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('groups.students.scores.index',[$group,$student])}}">Оценки
                    студента</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактирование оценок</li>
        </ol>

    </div>
    <div class="container">
        <h3>Изменение оценок</h3>

        <?php  $studentId = \App\Models\Score::where('student_id', '=', $student)->first();

        if ($studentId != null) {
            $placeScore1 = \App\Models\Score::where('id', '=', $scoreId1)->first()->score;
            $placeScore2 = \App\Models\Score::where('id', '=', $scoreId1 + 1)->first()->score;
            $placeScore3 = \App\Models\Score::where('id', '=', $scoreId1 + 2)->first()->score;
        } else {
            $placeScore1 = "Русский язык";
            $placeScore2 = "Математика";
            $placeScore3 = "История";
        }
        ?>


        <?php $score = collect(['', '', ''])?>
        <?php $subjectId = collect(['', '', ''])?>
        <form class="form-horizontal" action="{{ route('groups.students.scores.update', [$group,$student,$scoreId1])}}"
              method="post">
            <input type="hidden" name="_method" value="put">
            <input type="hidden" name="studentId" value="{{$student}}">
            {{csrf_field()}}
            <table class="table table-striped">
                <thead>
                <th>Русский язык</th>
                <th class="text-center">Математика</th>
                <th class="text-center">История</th>
                <th class="text-center"></th>
                </thead>
                <tbody>

                <tr>
                    <td>
                        <input type="text" class="form-control" name="score1" placeholder="{{$placeScore1}}"
                               value="{{$score[0]}}">
                        <input type="hidden" name="subjectId1" placeholder="" value="{{$subjectId[0]='1'}}">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="score2" placeholder="{{$placeScore2}}"
                               value="{{$score[1]}}">
                        <input type="hidden" name="subjectId2" placeholder="" value="{{$subjectId[1]='2'}}">
                    </td>
                    <td class="text-center">
                        <input type="text" class="form-control" name="score3" placeholder="{{$placeScore3}}"
                               value="{{$score[2]}}">
                        <input type="hidden" name="subjectId3" placeholder="" value="{{$subjectId[2]='3'}}">
                    </td>
                    <td class="text-right">
                        <input class="btn btn-primary" type="submit" value="Изменить оценки">
                    </td>
                </tr>
                </tbody>
            </table>
            <hr/>
        </form>
    </div>
@endsection