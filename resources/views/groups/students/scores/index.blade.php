@extends('layouts.app')


@section('content')

    <div class="container">

        <h2>Оценки студента</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('groups.index')}}">Группы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Оценки студента</li>
        </ol>

        <table class="table table-striped">
            <h1 class="text-center">{{$student->name}}</h1>
            <p class="text-center">{{$student->birthday}}</p>
            <thead>

            <th class=text-center>Русский язык</th>
            <th class="text-center">Математика</th>
            <th class="text-center">История</th>
            <th class="text-center">Средний балл</th>
            <th class="text-right">Изменить оценки</th>
            <th class="text-right">Удалить оценки</th>
            </thead>
            <tbody>

            <?php $sumb = '0';  $scoreId = []; $k = '0' ?>
            @forelse($student->scores as $score)
                <td class="text-center">{{$score->score}}</td>
                <?php $sumb = $sumb + ($score->score)?>
                <?php $scoreId[$k] = ($score->id); ++$k; ?>
            @empty
                <?php $scoreId = [];
                $scoreId[0] = '-'; ?>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
            @endforelse

            <td class="text-center"><?php if (count($student->scores) == '0') {
                    echo '-';
                } else {
                    echo round($sumb / count($student->scores), 1);
                }
                ?>
            </td>

            <td class="text-right">

                <a class="btn btn-default"
                   href="{{route('groups.students.scores.edit',[$group->id, $student->id,$scoreId[0]])}}"><i class="fa
                     fa-edit" aria-hidden="true"></i></a>
            </td>
            <td class="text-right">
                <form onsubmit="if(confirm('Удалить?')){return true}else{return false}"
                      action="{{route('groups.students.scores.destroy',[$group->id, $student->id,$scoreId[0]])}}"
                      method="post">
                    <input type="hidden" name="_method" value="delete">
                    {{csrf_field()}}
                    <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                </form>
            </td>
            </tr>
            </tbody>
        </table>


    </div>
    <div class="container">
        <h3>Добавление оценок</h3>
        <?php $score = collect(['', '', ''])?>
        <?php $subjectId = collect(['', '', ''])?>
        <form class="form-horizontal" action="{{ route('groups.students.scores.store', [$group->id,$student->id]) }}"
              method="post">
            <input type="hidden" name="studentId" value="{{$student->id}}">

            {{csrf_field()}}
            <table class="table table-striped">
                <thead>
                <th>Русский язык</th>
                <th class="text-center">Математика</th>
                <th class="text-center">История</th>
                <th class="text-center"></th>
                </thead>
                <tbody>

                <tr>
                    <td>
                        <input type="text" class="form-control" name="score1" placeholder="Русский язык"
                               value="{{$score[0]}}">
                        <input type="hidden" name="subjectId1" value="{{$subjectId[0]='1'}}">

                    </td>
                    <td>
                        <input type="text" class="form-control" name="score2" placeholder="Математика"
                               value="{{$score[1]}}">
                        <input type="hidden" name="subjectId2" value="{{$subjectId[1]='2'}}">
                    </td>
                    <td class="text-center">
                        <input type="text" class="form-control" name="score3" placeholder="История"
                               value="{{$score[2]}}">
                        <input type="hidden" name="subjectId3" value="{{$subjectId[2]='3'}}">
                    </td>
                    <td class="text-right">
                        <input class="btn btn-primary" type="submit" value="Добавить оценки">

                    </td>
                </tr>
                </tbody>
            </table>
            <hr/>
        </form>
    </div>
@endsection