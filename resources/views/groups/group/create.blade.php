<div class="сontainer">
    <br>
    <h2 class="text">Добавление группы</h2></div>


<form class="form-horizontal" action="{{route('groups.store')}}" method="post">
    {{csrf_field()}}
    <table class="table table-striped">
        <thead>
        <th class="text-center">Название группы</th>
        <th class="text-center">Описание</th>
        <th class="text-right">Добавить</th>
        </thead>
        <tbody>
        <tr>
            <td class="text-left"><input type="text" class="form-control" name="title"
                                         placeholder="Название группы"></td>
            <td class="text-center"><input type="text" class="form-control" name="description"
                                           placeholder="Описание"></td>
            <td class="text-right"><input class="btn btn-primary" type="submit" value="Добавить"></td>
        </tr>
        </tbody>
    </table>
    <hr/>
</form>
</div>
