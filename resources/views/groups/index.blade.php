@extends('layouts.app')


@section('content')
    <br>
    <div class="container">


        <h2>Список групп</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Группы</li>
        </ol>


        <table class="table table-striped">
            <thead>
            <th>Название группы</th>
            <th>Описание</th>
            <th class="text-center">Успеваемость</th>
            <th class="text-center">Ср. балл <br>Русский язык</th>
            <th class="text-center">Ср. балл <br>Математика</th>
            <th class="text-center">Ср. балл <br>История</th>
            <!-- <th class="text-right">Просмотр</th>
             <th class="text-right">Редактировать</th>-->
            @if (auth()->user() && (auth()->user()->role == 'admin'))
                <th class="text-right">Удалить</th>
            @endif

            </thead>
            <tbody>
            @forelse ($groups as $group)
                <tr>
                    <td class="text-left"> {{$group->title}}</td>
                    <td>{{$group->description}}</td>

                    <td class="text-center"><?php if ($group->groupMiddleScore($group, 4) == '0') {
                            echo '-';
                        } else {
                            echo($group->groupMiddleScore($group, 4));
                        }?></td>
                    <td class="text-center"><?php if ($group->groupMiddleScore($group, 1) == '0') {
                            echo '-';
                        } else {
                            echo($group->groupMiddleScore($group, 1));
                        }?></td>

                    <td class="text-center"><?php if ($group->groupMiddleScore($group, 2) == '0') {
                            echo '-';
                        } else {
                            echo($group->groupMiddleScore($group, 2));
                        }?></td>
                    <td class="text-center"><?php if ($group->groupMiddleScore($group, 3) == '0') {
                            echo '-';
                        } else {
                            echo($group->groupMiddleScore($group, 3));
                        }?></td>
                 <td class="text-right">
                        <a class="btn btn-default" href="{{route('groups.show', $group)}}"><i class="fa
                     fa-th-list"></i></a>
                    </td>

                <!-- <td class="text-right">
                        <a class="btn btn-default" href="{{route('groups.edit', $group)}}"><i class="fa
                     fa-edit"></i></a>
                    </td>-->
                    @if (auth()->user() && (auth()->user()->role == 'admin'))
                        <td class="text-right">
                            <form onsubmit="if(confirm('Удалить?')){return true}else{return false}"
                                  action="{{route('groups.destroy',$group)}}" method="post">
                                <input type="hidden" name="_method" value="delete">
                                {{csrf_field()}}

                                <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                            </form>
                        </td>
                    @endif
                </tr>
            @empty
            @endforelse
            </tbody>
        </table>
        <hr>
        <?php echo $groups->render(); ?>
        @if (auth()->user() && (auth()->user()->role == 'admin'))
            @include('groups.group.create')
        @endif
    </div>
@endsection