@extends('layouts.app')


@section('content')

    <div class="container">
        <h2>Список студентов</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('groups.index')}}">Группы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Студенты</li>
        </ol>
        <hr>
        <table class="table table">
            <h1 class="text-center">{{$group->title}}</h1>
            <p class="text-center">{{$group->description}}</p>
            <thead>
            <th>ФИО студента</th>
            <th>Дата рождения</th>
            <th class="text-center" background="red">Русский язык</th>
            <th class="text-center">Математsdasdasdsadsadика</th>
            <th class="text-center">История</th>
            <th class="text-center">Средний балл</th>
            <th class="text-center">Оценки</th>
            <th class="text-center">Редактировать</th>
            <th class="text-right">Удалить</th>
            </thead>
            <tbody>
            @forelse($students as $student)
                <?php $sumb = '0'?>
                @forelse($student->scores as $score)
                    <?php $sumb = $sumb + ($score->score)?>
                @empty
                @endforelse

                <?php if (count($student->scores) == '0') {
                    $k = '0';
                } else {
                    $k = round($sumb / count($student->scores), 1);
                }?>
                <!-- Перекрашивание ячеек -->
                <?php if ($k != '0' and $k == '5') {

                    $color = 'five';
                } elseif ('5' > $k and $k > '4.5') {

                    $color = 'four';
                } elseif ($k != '0' and $k < '3') {
                    $color = 'three';
                } else {
                    $color = 'default';
                }?>
                <tr class="{{$color}}">
                    <td>{{$student->name}}</td>
                    <?php $birthday = date('d.m.Y', strtotime($student->birthday));?>
                    <td>{{$birthday}}</td>
                    @forelse($student->scores as $score)
                        <td class="text-center">{{$score->score}}</td>
                    @empty
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                    @endforelse
                    <td class="text-center">
                        <?php if (count($student->scores) == '0') {
                            echo '-';
                        } else {
                            echo $k;
                        }?>
                    </td>
                    <td class="text-center">
                        <a class="btn btn-default"
                           href="{{route('groups.students.scores.index',[$group->id,$student->id])}}"><i class="fa
                     fa-edit"></i></a>
                    </td>
                    <td class="text-center">
                        <a class="btn btn-default"
                           href="{{route('groups.students.edit',[$group->id, $student->id])}}"><i class="fa
                     fa-edit" aria-hidden="true"></i></a>
                    </td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Удалить?')){return true}else{return false}"
                              action="{{route('groups.students.destroy',[$group->id, $student])}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            {{csrf_field()}}
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><h3 class="text-center">Студентов нет</h3></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <hr/>
        <td><a href="{{route('groups.students.create', [$group->id])}}" class="btn btn-primary pull-right">Добавить
                студента</a></td>
        <?php echo $students->render(); ?>
    </div>
@endsection