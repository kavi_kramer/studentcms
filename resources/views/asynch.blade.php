@extends('layouts.app')


@section('content')
<br>
<div class="container" id="content">
    <h2>Список групп</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Главная</a></li>
        <li class="breadcrumb-item active" aria-current="page">Группы</li>
    </ol>
@include('scripts.asynch_script')

</div>
@endsection