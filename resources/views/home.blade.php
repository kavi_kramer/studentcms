@extends('layouts.app')


@section('content')

    @include('home.filter')
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Главная страница</h2>
            </div>
            <div class="col-md-6">

            </div>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <th>Групп</th>
                <th>Студент</th>
                <th>Дата рождения</th>
                <th class="text-center">Русский язык</th>
                <th class="text-center">Математика</th>
                <th class="text-center">История</th>
                <th class="text-center">Успеваемость</th>
                @if (auth()->user() && (auth()->user()->role == 'admin'))
                    <th class="text-right">Анкета</th>
                    <th class="text-right">Удалить</th>
                @endif
                </thead>
                <tbody>
                @forelse ($students as $student)
                    <tr>

                        <td class="text-left"> {{$student->group->title}}</td>
                        <td class="text-left"> {{$student->name}}</td>
                        <td class="text-left"> {{$student->birthdayConvert($student->birthday)}}</td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 1) == null) {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 1));
                            }?></td>

                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 2) == null) {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 2));
                            }?></td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 3) == null) {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 3));
                            }?></td>
                        <td class="text-center"><?php if ($student->studentMiddleScore($student, 4) == '0') {
                                echo '-';
                            } else {
                                echo($student->studentMiddleScore($student, 4));
                            }?></td>

                        @if (auth()->user() && (auth()->user()->role == 'admin'))

                            <td class="text-right">
                                <a class="btn btn-default"
                                   href="{{route('adminGates', [$student->group_id, $student])}}"><i class="fa
                                     fa-edit"></i></a>
                            </td>
                            <td class="text-right">
                                <form onsubmit="if(confirm('Удалить?')){return true}else{return false}"
                                      action="{{route('groups.students.destroy',[$student->group_id, $student])}}"
                                      method="post">
                                    <input type="hidden" name="_method" value="delete">
                                    {{csrf_field()}}

                                    <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </td>
                        @endif
                    </tr>
                @empty
                @endforelse

            </table>
        </div>
        @include('paginate',['paginate'=>$students->appends('home')])

    </div>
@endsection

