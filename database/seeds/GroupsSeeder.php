<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Group;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Score;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sub = DB::table('subjects')->whereNull('created_at')->get();
        if ($sub != null) {
        DB::table('subjects')->insert([
            [   'id' => "1",
                'denomination' => "Русский язык",
            ],
            [   'id' => "2",
                'denomination' => "Математика",
            ],[
                'id' => "3",
                'denomination' => "История",
            ]]
        );
       }
        factory(Group::class,'group',4)->create()->each(function($student){
            $student->students()->save(factory(Student::class,'student')->make());
            $student = Student::latest()->get()->last();
            $student ->scores()->save(factory(Score::class,'score')->make());
});


    }
}
