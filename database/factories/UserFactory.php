<?php

use Faker\Generator as Faker;

use App\Models\Group;
use App\Models\Student;
use App\Models\Score;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->defineAs(Group::class,'group',function (Faker $faker){
        return[
                'title' => $faker->bothify('Group ##?'),
                'description'=>$faker->realText(50)
        ];
    });
$factory->defineAs(Student::class,'student',function (Faker $faker){
    return[
        'name' => $faker->name,
        'birthday'=>$faker->date($format = 'Y-m-d',$max = 'now'),
        'group_id'=>"group"
    ];
});
$factory->defineAs(Score::class,'score',function (Faker $faker){
    return[
        'score' => $faker->numberBetween($min = 3, $max = 4),
        'student_id'=>"student",
        'subject_id'=>$faker->numberBetween($min = 1, $max = 3)
    ];
});